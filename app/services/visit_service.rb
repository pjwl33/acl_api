module VisitService

  module IDService
    def self.find_visitors_by_document(document_type, document_id)
      documents = IdentityDocument.where(document_type: document_type, document_id: document_id)
      users = documents.map{|d| d.users}.flatten.uniq
    end

    def self.find_visits_by_document(document_type, document_id, headers, start_time=nil, end_time=nil)
      if document_type == 'Barcode'
        VisitService::Shortpath.find_visits_by_document(document_type, document_id, headers, start_time, end_time)
      else
        if document_type == 'PivCard'
          begin
            cert = R509::Cert.new(:cert => document_id)
            document_id = cert.fingerprint
          rescue
            # NOT CERT STR
          end
        elsif document_type == 'DriverLicense'
          document_id = document_id.gsub(/\s/, '')
        else
          # Not supported
        end
        users = find_visitors_by_document(document_type, document_id)
        emails = users.map{|u|u.email}
        VisitService::Shortpath.find_visits_by_email(emails.first, headers, start_time, end_time)
      end
    end
  end

  module Shortpath
    def self.find_visits_by_document(document_type, document_id, headers, start_time=nil, end_time=nil)
      params = {
        :document_type => document_type,
        :document_id => document_id,
        :start_time => start_time,
        :end_time => end_time
      }
      SPHttpService.get('api/security/visits', params, headers)
    end

    def self.find_visits_by_email(email, headers, start_time=nil, end_time=nil)
      self.find_visits_by_document('Email', email, headers, start_time, end_time)
    end

    def self.checkin(visit_id, data, headers)
      SPHttpService.post("api/security/visits/#{visit_id}", data, headers)
    end
  end

end