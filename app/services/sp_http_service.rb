class SPHttpService

  BASE_URI = "https://core.shortpath.com"
  # BASE_URI = "http://10.100.131.190:3000"
  HEADERS = {
    # 'Authorization'=> ENV['SHORTPATH_API_KEY'],
    # 'Accept' => 'application/json',
    # 'Content-Type' => 'application/json'
  }

  def self.get(route, query, headers)
    path = File.join(BASE_URI, route)
    path = URI::encode(path)
    resp = HTTParty.get(path, query: query, headers: HEADERS.merge(headers))
    body = resp.body
    json = JSON.parse(body)
    json = json.with_indifferent_access if json.respond_to?(:with_indifferent_access)
    json
  end

  def self.post(route, payload, headers)
    path = File.join(BASE_URI, route)
    path = URI::encode(path)
    resp = HTTParty.post(path, body: payload, headers: HEADERS.merge(headers))
    body = resp.body
    json = JSON.parse(body)
    json = json.with_indifferent_access if json.respond_to?(:with_indifferent_access)
    json
  end

end