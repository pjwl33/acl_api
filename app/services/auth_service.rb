module AuthService

  module Shortpath

    def self.check_user(headers)
      route = "api/users/details.json"
      SPHttpService.get(route, {}, headers)
    end

  end

end