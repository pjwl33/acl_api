module TenantService

  module Shortpath

    def self.search_directory(q, headers)
      route = "api/security/visits/directory.json?q=#{q}"
      SPHttpService.get(route, {}, headers)
    end

    def self.search_organizers(group_id, q, headers)
      route = "/events/search_organizers.json?group_id=#{group_id}&organizer_name=#{q}"
      SPHttpService.get(route, {}, headers)
    end

    def self.search_guests(group_id, q, headers)
      route = "/events/search_guests.json?group_id=#{group_id}"
      body = {
        contact: {
          name: q
        }
      }
      SPHttpService.get(route, body, headers)
    end

    def self.create_event(group_id, event, headers)
      headers.merge!({
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
      });
      route = "/groups/#{group_id}/events.json"
      body = event.to_json
      SPHttpService.post(route, body, headers)
    end

    def self.get_locations(group_id, headers)
      route = "/api/groups/#{group_id}/locations.json"
      SPHttpService.get(route, {}, headers)
    end
  end

end