class Api::LocationsController < ApplicationController

  def index
    @locations = TenantService::Shortpath.get_locations(tenant_id, http_basic_auth_header)
  end

  protected

  def tenant_id
    params[:tenant_id]
  end

end