class Api::AuthsController < ApplicationController

  def shortpath_user
    @user = AuthService::Shortpath.check_user(http_basic_auth_header)
  end

end