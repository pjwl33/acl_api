class Api::VisitorsController < ApplicationController

  def index
    @visitors = VisitService::IDService.find_visitors_by_document(doc_type, doc_id)
  end

  def validate_piv
    begin
      @cert = R509::Cert.new(:cert => document_id)
    rescue
      # NOT CERT STR
    end
  end

  protected

  def document_id
    params[:document_id]
  end

  def document_type
    params[:document_type]
  end

end