class Api::OrganizersController < ApplicationController

  def index
    @organizers = TenantService::Shortpath.search_organizers(tenant_id, query, http_basic_auth_header)
  end

  protected

  def tenant_id
    params[:tenant_id]
  end

  def query
    params[:q]
  end

end