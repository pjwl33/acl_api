class Api::SchedulesController < ApplicationController

  def create
    @event = TenantService::Shortpath.create_event(tenant_id, event_params, http_basic_auth_header)['event']
    render :show
  end

  protected

  def tenant_id
    params[:tenant_id]
  end

  def event_params
    params.permit!
  end

end