class Api::DirectoriesController < ApplicationController

  def show
    @users = TenantService::Shortpath.search_directory(query, http_basic_auth_header)
  end

  protected

  def query
    params[:q]
  end

end