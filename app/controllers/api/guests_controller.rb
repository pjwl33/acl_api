class Api::GuestsController < ApplicationController

  def index
    @guests = TenantService::Shortpath.search_guests(tenant_id, query, http_basic_auth_header)
  end

  protected

  def tenant_id
    params[:tenant_id]
  end

  def query
    params[:q]
  end

end