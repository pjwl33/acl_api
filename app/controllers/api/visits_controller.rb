class Api::VisitsController < ApplicationController

  def index
    find_visits
    render :index
  end

  def create
    find_visits
    render :index
  end

  protected

  def find_visits
    doc_type = params[:document_type]
    doc_id = params[:document_id]
    @visits = VisitService::IDService.find_visits_by_document(doc_type, doc_id, http_basic_auth_header)
  end

end