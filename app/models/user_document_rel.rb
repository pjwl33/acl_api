# == Schema Information
#
# Table name: user_document_rels
#
#  id                   :integer          not null, primary key
#  user_id              :integer          not null
#  identity_document_id :integer          not null
#  status               :string(255)
#  deleted_at           :datetime
#  created_at           :datetime
#  updated_at           :datetime
#

class UserDocumentRel < ActiveRecord::Base
  acts_as_paranoid
  has_paper_trail
  
  belongs_to :user
  belongs_to :identity_document

end
