# == Schema Information
#
# Table name: identity_documents
#
#  id            :integer          not null, primary key
#  document_id   :string(255)      not null
#  document_type :string(255)      not null
#  first_name    :string(255)
#  middle_name   :string(255)
#  last_name     :string(255)
#  name          :string(255)
#  dob           :date
#  barcode       :string(255)
#  deleted_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#

class IdentityDocument < ActiveRecord::Base
  acts_as_paranoid
  has_paper_trail

  has_many :user_document_rels
  has_many :users, through: :user_document_rels
  has_many :identity_document_images
  
end
