json.user do
  user = @user['user']
  json.id user['id']
  json.login user['login']
  json.email user['email']
  json.first_name user['first_name']
  json.last_name user['last_name']
  json.name user['name']
  json.contact_id user['contact_id']
  json.title user['title']
  json.phone_number user['phone_number']
  json.state user['state']
end

json.groups @user['groups'] do |group|
  json.id group['id']
  json.name group['name']
  json.hostname group['hostname']
end