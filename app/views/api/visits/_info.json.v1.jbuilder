json.id visit['visit']['id']
json.barcode visit['visit']['barcode']
json.visit_start_time Time.parse(visit['visit']['start_time']).try(:to_i)
json.visit_end_time Time.parse(visit['visit']['end_time']).try(:to_i)
json.visitor do
  json.id visit['visit']['visitor']['id']
  json.first_name visit['visit']['visitor']['first_name']
  json.last_name visit['visit']['visitor']['last_name']
  json.name visit['visit']['visitor']['name']
end
json.tenant do
  json.id visit['visit']['tenant']['id']
  json.name visit['visit']['tenant']['name']
  json.suite visit['visit']['space']['name']
  json.floor visit['visit']['floor']['name']
end
json.tenant_contact do
  json.id visit['visit']['tenant_contact']['id']
  json.first_name visit['visit']['tenant_contact']['first_name']
  json.last_name visit['visit']['tenant_contact']['last_name']
  json.name visit['visit']['tenant_contact']['name']
end
