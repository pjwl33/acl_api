json.array! @locations do |building|
  json.building do
    json.name building[0]
    json.locations building[1] do |location|
      loc = location.values.first
      type = location.keys.first
      json.id loc['id']
      json.name loc['name']
      json.type type
    end
  end
end