class CreateIdentityDocuments < ActiveRecord::Migration
  def change
    create_table :identity_documents do |t|
      t.string :document_id, null: false
      t.string :document_type, null: false

      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :name
      t.date :dob
      t.string :barcode

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :identity_documents, [:document_id, :document_type, :deleted_at], unique: true, name: 'index_document'
  end
end
