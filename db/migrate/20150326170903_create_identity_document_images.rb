class CreateIdentityDocumentImages < ActiveRecord::Migration
  def change
    create_table :identity_document_images do |t|

      t.integer :identity_document_id, null: false
      t.string :image_type
      t.attachment :file

      t.datetime :deleted_at
      t.timestamps
    end

    add_index :identity_document_images, [:identity_document_id, :deleted_at], name: 'index_document_id'
  end
end
