class CreateUserDocumentRels < ActiveRecord::Migration
  def change
    create_table :user_document_rels do |t|
      t.integer :user_id, null: false
      t.integer :identity_document_id, null: false
      t.string :status

      t.datetime :deleted_at
      t.timestamps
    end
    add_index :user_document_rels, [:user_id, :identity_document_id, :deleted_at], unique: true, name: 'index_user_document'
  end
end
