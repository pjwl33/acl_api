# == Schema Information
#
# Table name: user_document_rels
#
#  id                   :integer          not null, primary key
#  user_id              :integer          not null
#  identity_document_id :integer          not null
#  status               :string(255)
#  deleted_at           :datetime
#  created_at           :datetime
#  updated_at           :datetime
#

require 'rails_helper'

RSpec.describe UserDocumentRel, :type => :model do
  it 'has a valid factory' do
    expect(build(:user_document_rel)).to be_valid
  end
end
