# == Schema Information
#
# Table name: identity_document_images
#
#  id                   :integer          not null, primary key
#  identity_document_id :integer          not null
#  image_type           :string(255)
#  file_file_name       :string(255)
#  file_content_type    :string(255)
#  file_file_size       :integer
#  file_updated_at      :datetime
#  deleted_at           :datetime
#  created_at           :datetime
#  updated_at           :datetime
#

FactoryGirl.define do
  factory :identity_document_image do
    association :identity_document, factory: :identity_document
  end

end
