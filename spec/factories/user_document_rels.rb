# == Schema Information
#
# Table name: user_document_rels
#
#  id                   :integer          not null, primary key
#  user_id              :integer          not null
#  identity_document_id :integer          not null
#  status               :string(255)
#  deleted_at           :datetime
#  created_at           :datetime
#  updated_at           :datetime
#

FactoryGirl.define do
  factory :user_document_rel do
    association :user, factory: :user
    association :identity_document, factory: :identity_document
  end

end
