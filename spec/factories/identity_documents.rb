# == Schema Information
#
# Table name: identity_documents
#
#  id            :integer          not null, primary key
#  document_id   :string(255)      not null
#  document_type :string(255)      not null
#  first_name    :string(255)
#  middle_name   :string(255)
#  last_name     :string(255)
#  name          :string(255)
#  dob           :date
#  barcode       :string(255)
#  deleted_at    :datetime
#  created_at    :datetime
#  updated_at    :datetime
#

FactoryGirl.define do
  factory :identity_document do
    sequence(:document_id) { |n| "doc#{n}" }
    document_type "DriverLicense"
    first_name "John"
    last_name "Smith"
    name "John Smith"
  end

end
