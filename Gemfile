source 'https://rubygems.org'

gem 'rails', '4.1.6'
gem 'mysql2'

# Assets
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'sdoc', '~> 0.4.0',          group: :doc

# API related
gem 'jbuilder'
gem 'versioncake'
gem 'httparty'

# Config
gem 'dotenv-rails'

# Use unicorn as the app server
gem 'unicorn'

# authentication
gem 'devise'

# Error tracking
gem 'airbrake'

# Print object nicely
gem 'awesome_print', :require => 'ap'

# Cert decoding
gem 'r509', :git => 'git://github.com/BuildingIntelligence/r509.git'

# File store
gem 'aws-sdk', '~> 1.5.7'
gem 'paperclip', '~> 4.2'

# Audit
gem 'paper_trail', '~> 3.0.6'

# Soft delete
gem 'paranoia', '~> 2.1.1'

group :development do
  # Debug tools
  gem 'better_errors'
  gem 'binding_of_caller' # together with better_errors enables instance variable inspection
end

group :development, :test do
  # Test suite
  gem 'rspec-rails', '~> 3.0.0'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
end