window.app = {
    setStore: function(key, obj) {
        var objType = typeof(obj);
        var storeVal = obj;
        if (objType == 'object') {
            storeVal = JSON.stringify(obj);
        }
        localStorage.setItem(key, storeVal);
    },
    getStore: function(key) {
        var storeVal = localStorage.getItem(key);
        var obj = null;
        try {
            obj = JSON.parse(storeVal);
        } catch (e) {
            obj = storeVal;
        }
        return obj;
    },
    removeStore: function(key) {
        localStorage.removeItem(key);
    },
    clearInfo: function() {
        for (var k in localStorage) {
            if (k != 'app_config' && k != 'remember_signin') {
                this.removeStore(k);
            }
        }
    },
    currentUser: function() {
        var self = this;
        var current_user_attr = self.getStore('current_user_attr');
        if (current_user_attr) {
            return current_user_attr;
        }
        return null;
    }
};

window.net = {
    setupAjax: function(opts) {
        var userInfo = app.getStore('current_user');
        opts.beforeSend = function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(userInfo.username + ":" + userInfo.password));
        };
    },
    get: function(opts) {
        opts.type = "GET";
        this.setupAjax(opts);
        $.ajax(opts);
    },
    post: function(opts) {
        opts.type = "POST";
        this.setupAjax(opts);
        $.ajax(opts);
    }
};