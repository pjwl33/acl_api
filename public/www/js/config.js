window.AppConf = function() {
    // default AppConf, QR code scanning will reconfigure this object
    var defaultAppConf = {
        env: 'rest',
        // host: 'https://m.vscworldtradecenter.com/',
        host: '/',
        // mqUrl: 'tcp://staging1.bi.sv3.us:1883',
    };
    var appConfig = app.getStore('app_config');
    if (appConfig === null) appConfig = {};
    var c = $.extend({}, defaultAppConf, appConfig);
    return c;
}();