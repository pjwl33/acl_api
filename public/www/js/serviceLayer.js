window.AppService = {
    fetchRoutes: function(currentUser, currentGroup, vendors, buildings) {
        return {
            // main nav links
            root: '/',
            home: '/groups/' + currentGroup.id,
            maintenance: '/pm',
            deliveries: '/deliveries',
            service_overview: '/vendors/' + _.first(vendors).id + '/overview',
            service_overviews: '/vendors/',
            group: '/groups/' + currentGroup.id,
            groups: '/groups/',
            building: '/buildings/' + _.first(buildings).id,
            buildings: '/buildings/',
            // profile links
            profile: '/users/' + currentUser.id,
            profile_edit: '/users/' + currentUser.id + '/edit/',
            sign_out: '/logout',
            sign_in: '/sessions/new',
            // admin links
            admin_users: '/admin/users',
            admin_groups: '/groups',
            admin_buildings: '/buildings',
            admin_vendors: '/vendors',
            admin_orders: '/service_requests',
            admin_verifications: '/verification_requests',
            admin_subscriptions: '/subscriptions',
            admin_subdomains: '/subdomains',
            admin_audit: '/audits',
            // mgmt links
            calendar: '/groups/' + currentGroup.id + '/events',
            visitors: '/groups/' + currentGroup.id + '/visitors',
            contacts: '/groups/' + currentGroup.id + '/contacts',
            placed_orders: '/groups/' + currentGroup.id + '/orders',
            vendors: '/groups/' + currentGroup.id + '/vendors',
            settings: '/groups/' + currentGroup.id + '/edit',
            // temp events show route
            show_event: '/groups/' + currentGroup.id + '/events/'
        };
    },
    fetchUserData: function(options, success, error) {
        if (AppConf.env == 'test') {
            // user data/roles
            data = {
                vendors: [{
                    name: "Shortpath Account Support ",
                    id: 4143
                }],
                user: {
                    email: "tianyu@buildingintelligence.com",
                    first_name: "Tianyu1",
                    contact_id: 154608,
                    phone_number: "",
                    title: "",
                    company: "BI",
                    login: "tianyu",
                    name: "Tianyu1 Huang",
                    state: "active",
                    last_name: "Huang",
                    id: 5512
                },
                locations: [{
                    address: "360 Lexington Ave, New York, NY",
                    name: "360 Lexington Avenue",
                    id: 1312
                }, {
                    address: "1617 JFK Boulevard , Philadelphia, PA",
                    name: "One Penn Center",
                    id: 2441
                }],
                groups: [{
                    group: {
                        primary_contact_id: 3,
                        updated_at: "2014-04-13T03:54:13-04:00",
                        features: 0,
                        billing_address_id: null,
                        id: 99,
                        hostname: "",
                        deleted_at: null,
                        updated_by_user_id: 3,
                        description: "",
                        created_by_user_id: 3,
                        version: null,
                        join: 0,
                        primary_address_id: 17881,
                        admin_approve: false,
                        name: "Building Intelligence Inc.",
                        created_at: "2009-06-29T11:02:06-04:00"
                    }
                }]
            };
            success(data);
        } else if (AppConf.env == 'rest') {
            // should return the current user OR null
            net.get({
                url: AppConf.host + 'api/auth/shortpath_user.json',
                success: function(data, textStatus, jqXHR) {
                    success(data, textStatus, jqXHR);
                },
                error: function(data, textStatus) {
                    error(data, textStatus);
                }
            });
        }
    },
    fetchLocations: function(options, success, error) {
        if (AppConf.env == 'test') {
            data = [{
                building: {
                    name: "City Towers",
                    locations: [{
                        id: 123,
                        name: "Suite 100",
                        type: "Space"
                    }, {
                        id: 125,
                        name: "Suite 2015",
                        type: "Space"
                    }]
                }
            }, {
                building: {
                    name: "Pizza Palace",
                    locations: [{
                        id: 5565,
                        name: "Pizza Palor",
                        type: "Space"
                    }, {
                        id: 66568,
                        name: "Basement 360",
                        type: "Space"
                    }]
                }
            }];
            success(data);
        } else if (AppConf.env == 'rest') {
            net.get({
                url: AppConf.host + 'api/tenants/' + options.tenant_id + '/locations.json',
                success: function(data, textStatus, jqXHR) {
                    success(data, textStatus, jqXHR);
                },
                error: function(data, textStatus) {
                    error(data, textStatus);
                }
            });
        }
    },
    searchGuests: function(options, success, error) {
        if (AppConf.env == 'test') {
            data = [{
                "contact_id": 204648,
                "first_name": "Paula",
                "last_name": "Dean",
                "email": "pd@cook.com",
                "company": "Sweets R' Us"
            }];
            success(data);
        } else if (AppConf.env == 'rest') {
            net.get({
                url: AppConf.host + 'api/tenants/' + options.tenant_id + '/guests.json?q=' + options.query,
                success: function(data, textStatus, jqXHR) {
                    success(data, textStatus, jqXHR);
                },
                error: function(data, textStatus) {
                    error(data, textStatus);
                }
            });
        }
    },
    searchOrganizers: function(options, success, error) {
        if (AppConf.env == 'test') {

        } else if (AppConf.env == 'rest') {
            net.get({
                url: AppConf.host + 'api/tenants/' + options.tenant_id + '/organizers.json?q=' + options.query,
                success: function(data, textStatus, jqXHR) {
                    success(data, textStatus, jqXHR);
                },
                error: function(data, textStatus) {
                    error(data, textStatus);
                }
            });
        }
    },
    createEvent: function(options, success, error) {
        if (AppConf.env == 'test') {
            console.log(options.group_id);
            console.log(options.data);
        } else if (AppConf.env == 'rest') {
            console.log(options.data);
            net.post({
                url: AppConf.host + 'api/tenants/' + options.tenant_id + '/schedules.json',
                dataType: 'JSON',
                contentType: 'application/json',
                data: JSON.stringify(options.data),
                success: success,
                error: error
            });
        }
    },
    createAccount: function(options, success, error) {
        if (AppConf.env == 'test') {
            console.log(options.data);
            success(options.data);
        } else if (AppConf.env == 'rest') {
            net.post({
                url: AppConf.host + 'api/visitors/create_account.json',
                dataType: 'JSON',
                contentType: 'application/json',
                data: JSON.stringify(options.data),
                success: success,
                error: error
            });
        }
    }
};