var TIMES_SELECT = [
  [0, '12:00 AM'],
  [0.5, '12:30 AM'],
  [1, '1:00 AM'],
  [1.5, '1:30 AM'],
  [2, '2:00 AM'],
  [2.5, '2:30 AM'],
  [3, '3:00 AM'],
  [3.5, '3:30 AM'],
  [4, '4:00 AM'],
  [4.5, '4:30 AM'],
  [5, '5:00 AM'],
  [5.5, '5:30 AM'],
  [6, '6:00 AM'],
  [6.5, '6:30 AM'],
  [7, '7:00 AM'],
  [7.5, '7:30 AM'],
  [8, '8:00 AM'],
  [8.5, '8:30 AM'],
  [9, '9:00 AM'],
  [9.5, '9:30 AM'],
  [10, '10:00 AM'],
  [10.5, '10:30 AM'],
  [11, '11:00 AM'],
  [11.5, '11:30 AM'],
  [12, '12:00 PM'],
  [12.5, '12:30 PM'],
  [13, '1:00 PM'],
  [13.5, '1:30 PM'],
  [14, '2:00 PM'],
  [14.5, '2:30 PM'],
  [15, '3:00 PM'],
  [15.5, '3:30 PM'],
  [16, '4:00 PM'],
  [16.5, '4:30 PM'],
  [17, '5:00 PM'],
  [17.5, '5:30 PM'],
  [18, '6:00 PM'],
  [18.5, '6:30 PM'],
  [19, '7:00 PM'],
  [19.5, '7:30 PM'],
  [20, '8:00 PM'],
  [20.5, '8:30 PM'],
  [21, '9:00 PM'],
  [21.5, '9:30 PM'],
  [22, '10:00 PM'],
  [22.5, '10:30 PM'],
  [23, '11:00 PM'],
  [23.5, '11:30 PM']
];

var DURATION = [
  ['All day', 0],
  ['1 hour', 1],
  ['2 hours', 2],
  ['3 hours', 3],
  ['4 hours', 4],
  ['5 hours', 5],
  ['6 hours', 6],
  ['7 hours', 7],
  ['8 hours', 8],
  ['9 hours', 9],
  ['10 hours', 10]
];

var REPEATS = [
  ["Does not repeat", 0],
  ["Weekly", 2],
  ["Monthly", 3],
  ["Yearly", 4]
];

var REPEAT_EVERY = [];
for (var i = 1; i <= 30; i++) {
  REPEAT_EVERY.push(i);
}

var REPEAT_DAYS = [
  {full: 'sunday', short: 'Sun'},
  {full: 'monday', short: 'Mon'},
  {full: 'tuesday', short: 'Tue'},
  {full: 'wednesday', short: 'Wed'},
  {full: 'thursday', short: 'Thu'},
  {full: 'saturday', short: 'Sat'},
];

var MONTHS = [{
  month: 'January',
  value: 1,
}, {
  month: 'February',
  value: 2,
}, {
  month: 'March',
  value: 3,
}, {
  month: 'April',
  value: 4,
}, {
  month: 'May',
  value: 5,
}, {
  month: 'June',
  value: 6,
}, {
  month: 'July',
  value: 7,
}, {
  month: 'August',
  value: 8,
}, {
  month: 'September',
  value: 9,
}, {
  month: 'October',
  value: 10,
}, {
  month: 'November',
  value: 11,
}, {
  month: 'December',
  value: 12,
}];

var DATES = [];
var YEARS = [];

for (var i = 1; i <= 31; i++) {
  DATES.push(i);
}

for (var i = 1900; i <= moment().year(); i++) {
  YEARS.push(i);
}
