// ASSURTEC DRIVER LICENSE SCANNER
ang.factory('dlScanner', function($rootScope) {
  var socket = null;
  var timer = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    },
    last: function() {
      return last_scan;
    },
    connect: function() {
      if (typeof io === "undefined" || null === io)
        throw "Socket.io not found!";
      socket = null;
      try {
        socket = io.connect(AppConf.dln_url);
      } catch (e) {
        console.log("Connection Failed");
      }
      return socket;
    },
    getSocket: function() {
      return socket;
    },
    start: function() {
      socket.emit('idscanctl', {
        command: 'start'
      });
    },
    stop: function() {
      socket.emit('idscanctl', {
        command: 'stop'
      });
    },
    status: function() {
      socket.emit('idscanctl', {
        command: 'status'
      });
    },
    restart: function() {
      socket.emit('idscanctl', {
        command: 'stop'
      });
      timer = setTimeout(function() {
        socket.emit('idscanctl', {
          command: 'start'
        });
      }, 3200);
    },
    removeAllListeners: function() {
      socket.removeAllListeners();
      clearTimeout(timer);
    }
  };
});

// ASSURTEC DRIVER LICENSE SCANNER
ang.factory('pivScanner', function($rootScope) {
  var socket = null;
  var timer = null;
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    },
    last: function() {
      return last_scan;
    },
    connect: function() {
      if (typeof io === "undefined" || null === io)
        throw "Socket.io not found!";
      socket = null;
      try {
        socket = io.connect(AppConf.piv_url);
      } catch (e) {
        console.log("Connection Failed");
      }
      return socket;
    },
    getSocket: function() {
      return socket;
    },
  };
});