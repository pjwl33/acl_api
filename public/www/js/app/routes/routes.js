ang.config(['$routeProvider', function($routeProvider){
  $routeProvider.
  when('/', {
    templateUrl: '../html/new_event.html',
    controller: 'NewEventCtrl'
  }).
  when('/visitor/sign_up', {
    templateUrl: '../html/visitors/sign_up.html',
    controller: 'VisitorSignupCtrl'
  }).
  when('/visitor/create_account', {
    templateUrl: '../html/visitors/create_account.html',
    controller: 'VisitorCreateAccount'
  }).
  otherwise({
    redirectTo: '/'
  });
}]);



