ang.controller('NewEventCtrl', function($scope, $routeParams, $timeout, $interval, $http) {

    // DATA initialized
    $scope.newEvent = {};
    $scope.contact = {};
    $scope.organizer = {};
    $scope.userLoginInfo = {};
    $scope.newEvent.organizers = [];
    $scope.newEvent.guests = [];
    $scope.orgSearchResults = [];
    $scope.guestSearchResults = [];

    if (app.getStore('current_user') && app.getStore('current_group')) {
        $scope.showLogin = false;
        $scope.currentGroup = app.getStore('current_group');
        getUserData();
    } else {
        $scope.showLogin = true;
    }

    function getUserData() {
        AppService.fetchUserData({}, function(data) {
            console.log(data);
            $scope.$apply(function() {
                $scope.user = data.user;
                $scope.groups = data.groups;
                $scope.choosingGroup = true;
            });
            $scope.newEvent.organizers.push({
                id: data.user.id,
                name: data.user.name,
                email: data.user.email
            });
            if ($scope.currentGroup) getLocations();
            checkCurrentTime();
        }, function(data, errorText) {
            alert('ERROR ' + data.status + ': please check your login information.');
            $scope.signOut();
        });
    }

    // DATE & TIME OPTIONS (ajax requests need 'MM/dd/yyyy' format)
    $scope.dateFormat = 'MMMM dd, yyyy';
    $scope.minDate = new Date();
    $scope.newEvent.range_start = moment().format('LL');
    $scope.newEvent.range_end = (moment().add(1, 'days')).format('LL');
    $scope.timeSelect = TIMES_SELECT;
    $scope.durationSelect = DURATION;
    $scope.repeatSelect = REPEATS;
    $scope.repeatEvery = REPEAT_EVERY;
    $scope.repeatDays = REPEAT_DAYS;

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.endOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.endOpened = true;
    };

    $scope.checkRepeatOptions = function() {
        var r = $scope.newEvent.repeats;
        if (r == 2) {
            $scope.repeatText = 'weeks';
            $scope.newEvent.range_end = (moment().add(7, 'days')).format('LL');
        } else if (r == 3) {
            $scope.repeatText = 'months';
            $scope.newEvent.range_end = (moment().add(30, 'days')).format('LL');
        } else if (r == 4) {
            $scope.repeatText = 'years';
            $scope.newEvent.range_end = (moment().add(365, 'days')).format('LL');
        } else {
            $scope.newEvent.range_end = (moment().add(1, 'days')).format('LL');
        }
    };

    function checkCurrentTime() {
        var currentHour = parseInt(moment().format('h')),
            currentMinutes = moment().format('mm'),
            currentAMPM = moment().format('A'),
            convertedHour = 30 - currentMinutes >= 0 ? currentHour : currentHour + 1,
            convertedMinutes = 30 - currentMinutes >= 0 ? 0.5 : 0,
            convertedAMPM = currentAMPM == 'AM' ? 0 : 12,
            closestIndex = convertedHour + convertedMinutes + convertedAMPM;

        $scope.newEvent.start_time = closestIndex;
        $scope.closestTime = closestIndex;
    }

    // Guests & Organizers
    $scope.removeOrganizer = function(index, o) {
        $scope.newEvent.organizers.splice(index, 1);
    };

    $scope.removeGuest = function(index, g) {
        $scope.newEvent.guests.splice(index, 1);
    };

    $scope.addGuest = function(item, model, label) {
        if (item.contact_id) {
            $scope.newEvent.guests.push(item);
        } else {
            if (item.first_name && item.email) {
                $scope.newEvent.guests.push(item);
            } else {
                alert("Guests must have a name and email");
            }
        }
        $scope.contact = {};
    };

    $scope.addOrganizer = function(item, model, label) {
        $scope.newEvent.organizers.push(item);
        $scope.organizer.name = undefined;
    };

    $scope.searchOrganizer = function(query) {
        $scope.loadingOrganizers = true;
        AppService.searchOrganizers({
            query: query,
            tenant_id: $scope.currentGroup.id
        }, function(data) {
            console.log(data);
            $scope.loadingOrganizers = false;
            $scope.$apply(function() {
                $scope.orgSearchResults = data;
            });
        }, function(data, errorText) {
            $scope.loadingOrganizers = false;
            alert('ERROR ' + data.status + ': ' + data.statusText);
        });
    };

    // temp ajax solution for guests
    $scope.$watch('contact.first_name', function(val) {
        if (val) {
            if ($scope.filterTextTimeout) $timeout.cancel($scope.filterTextTimeout);
            $scope.tempFilterText = val;
            $scope.filterTextTimeout = $timeout(function() {
                $scope.filterQuery = $scope.tempFilterText;
                $scope.searchGuest($scope.filterQuery);
            }, 250);
        }
    });

    $scope.$watch('organizer.name', function(val) {
        if (val) {
            if ($scope.filterTextTimeout) $timeout.cancel($scope.filterTextTimeout);
            $scope.tempFilterText = val;
            $scope.filterTextTimeout = $timeout(function() {
                $scope.filterQuery = $scope.tempFilterText;
                $scope.searchOrganizer($scope.filterQuery);
            }, 250);
        }
    });

    $scope.searchGuest = function(query) {
        $scope.loadingGuests = true;
        AppService.searchGuests({
            query: query,
            tenant_id: $scope.currentGroup.id
        }, function(data) {
            console.log(data);
            $scope.loadingGuests = false;
            $scope.noGuests = data.length > 0 ? false : true;
            $scope.$apply(function() {
                $scope.guestSearchResults = data;
            });
        }, function(data, errorText) {
            $scope.loadingGuests = false;
            alert('ERROR ' + data.status + ': ' + data.statusText);
        });
    };

    // GUEST CSV FUNCTIONS
    $scope.guestListTemplateUrl = '/events/guest_list_template.xlsx';

    $scope.uploadGuestList = function() {
        $scope.showUploadOptions = true;
    };

    // AJAX calls for changes & validations
    $scope.buttonText = "Add Event";
    $scope.createEvent = function(newEvent) {
        var data = parseEvent(newEvent);
        $scope.ajaxErrors = checkErrors(data);
        if ($scope.ajaxErrors.length > 0) return;
        $scope.buttonText = "Saving...";
        $scope.saving = true;
        AppService.createEvent({
            tenant_id: $scope.currentGroup.id,
            data: {
                event: data.event,
                event_guest_ids: data.event_guest_ids,
                guest_list: newEvent.guestList,
                organizer_name: $scope.user.name
            }
        }, function(data) {
            console.log(data);
            alert("Event successfully created!");
            location.reload();
        }, function(data, errorText) {
            $scope.saving = false;
            $scope.buttonText = "Add Event";
        });
    };

    function checkErrors(newEvent) {
        var ajaxErrors = [];
        for (var prop in newEvent) {
            if (prop == 'location_id' || prop == 'range_start' || prop == 'range_end' || prop == 'starts_at_time' || prop == 'starts_at_date') {
                if (!newEvent[prop]) ajaxErrors.push({
                    type: prop.split('_').join(' '),
                    description: prop.split('_').join(' ') + " cannot be blank"
                });
                console.log(prop);
            }
        }
        return ajaxErrors;
    }

    function parseEvent(newEvent) {
        var e = newEvent;
        var guestIds = [];
        var newGuests = {};

        _.map(e.guests, function(g) {
            if (g.contact_id) {
                guestIds.push(g.contact_id);
            } else {
                var key = Math.floor(new Date().getTime() / 1000);
                newGuests[key] = {
                    name: g.first_name,
                    email: g.email,
                    company: g.company
                };
            }
        });

        var data = {};
        data.event = {
            subject: e.subject ? e.subject : "New Event for " + $scope.currentGroup.name,
            starts_at_date: moment(e.range_start).format('L'),
            starts_at_time: e.start_time,
            duration: e.duration ? e.duration : 1,
            repeats: e.repeats ? e.repeats : 0,
            repeat_every: e.repeat_every ? e.repeat_every : 1,
            sunday: e.sunday ? e.sunday : 0,
            monday: e.monday ? e.monday : 0,
            tuesday: e.tuesday ? e.tuesday : 0,
            wednesday: e.wednesday ? e.wednesday : 0,
            thursday: e.thursday ? e.thursday : 0,
            friday: e.friday ? e.friday : 0,
            saturday: e.saturday ? e.saturday : 0,
            range_start: moment(e.range_start).format('L'),
            range_end: moment(e.range_end).format('L'),
            location_id: e.location_id,
            description: e.description,
            guests_attributes: newGuests,
            organizer_ids: _.map(e.organizers, function(o) {
                return o.user_id || o.id;
            }),
            notify_on_checkin: 0
        };
        data.event_guest_ids = guestIds;
        return data;
    }

    // TOGGLE functions
    $scope.openOrganizers = function() {
        $scope.showOrganizers = !$scope.showOrganizers;
    };

    $scope.openGuests = function() {
        $scope.showGuests = !$scope.showGuests;
    };

    // GET DATA
    function getLocations() {
        if ($scope.currentGroup.id) {
            AppService.fetchLocations({
                tenant_id: $scope.currentGroup.id
            }, function(data) {
                $scope.$apply(function() {
                    $scope.locations = data;
                });
            }, function(data, errorText) {
            });
        }
    }

    $scope.setLocation = function(newEvent) {
        $scope.newEvent.location_id = newEvent.location_id;
    };

    $scope.setGroup = function(group) {
        $scope.currentGroup = group;
        app.setStore('current_group', group);
        getLocations();
        $scope.choosingGroup = false;
        $scope.showLogin = false;
    };

    // LOGIN FUNCTIONS
    $scope.login = function(userInfo) {
        app.setStore('current_user', userInfo);
        getUserData();
    };

    $scope.signOut = function() {
        app.setStore('current_user', null);
        app.setStore('current_group', null);
        location.reload();
    };

});