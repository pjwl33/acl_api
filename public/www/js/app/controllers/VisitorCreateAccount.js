ang.controller('VisitorCreateAccount', function($scope, $timeout, $interval) {

  // find the visitor according to email code link

  $timeout(function() {
    Animations.inputsInit();
  });

  // AppService.fetchUserInfo({
      $scope.visitor = {
        id: 123485,
        email: "paul@sv3.us",
        company: "Building Intelligence",
        name: "Paul Lee"
      };
  // })

  $scope.createAccount = function(password, passwordConfirmation) {
    if (password !== passwordConfirmation) {
      $scope.errorText = "Passwords do not match. Please check to make sure they are the same.";
      $scope.accountCreated = false;
    } else {
      AppService.createAccount({
        data: {
          password: password,
          email: $scope.visitor.email,
          user_id: $scope.visitor.id
        }
      }, function(data) {
        $scope.errorText = "Account successfully created for " + $scope.visitor.email;
        $scope.accountCreated = true;
      }, function(data, errorText) {
        $scope.errorText = "Error " + data.status + ": there was an error creating the account.";
        $scope.accountCreated = false;
      });
    }
  };

  $scope.$on('$destroy', function() {
    $('body').children().unbind();
  });

});