ang.controller('VisitorSignupCtrl', function($scope, $timeout, $interval, $window) {

  app.setStore('current_user', {
    username: 'arminv',
    password: 'shortpath'
  });

  $scope.query = '';
  $timeout(function() {
    Animations.searchInit();
  });

  $scope.showOverlay = function() {
    $scope.overlay = true;
  };

  $scope.closeOverlay = function() {
    $scope.overlay = false;
  };

  $scope.selectTenant = function(t) {
    console.log(t);
    // filter by tenant_id here and pass as option
    $scope.tenant = t;
  };

  $scope.selectVisit = function(v) {
    console.log(v);
    app.setStore('past_visit', false);
  };

  $scope.$on('$destroy', function() {
    $('body').children().unbind();
  });

});