ang.directive('mainHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/mainHeader.html'
    };
});

ang.directive('lowerNav', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/lowerNav.html'
    };
});

ang.directive('whenField', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/whenField.html'
    };
});

ang.directive('eventInfo', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/eventInfo.html'
    };
});

ang.directive('organizers', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/organizers.html'
    };
});

ang.directive('guests', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/guests.html'
    };
});

ang.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                };
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    };
}]);

// ang.directive('selectionList', function() {
//     return {
//         restrict: 'E',
//         templateUrl: 'partials/selectionList.html'
//     };
// });

// ang.directive('ngBack', function($window) {
//     return function(scope, element, attrs) {
//         return element.bind('click', function() {
//             return scope.customBack ? scope.customBack() : $window.history.back();
//         });
//     };
// });

// angular.module('angularSlideables', [])
// .directive('slideable', function () {
//     return {
//         restrict:'C',
//         compile: function (element, attr) {
//             // wrap tag
//             var contents = element.html();
//             element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

//             return function postLink(scope, element, attrs) {
//                 // default properties
//                 attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
//                 attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
//                 element.css({
//                     'overflow': 'hidden',
//                     'height': '0px',
//                     'transitionProperty': 'height',
//                     'transitionDuration': attrs.duration,
//                     'transitionTimingFunction': attrs.easing
//                 });
//             };
//         }
//     };
// })
// .directive('slideToggle', function() {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             var target, content;

//             attrs.expanded = false;

//             element.bind('click', function() {
//                 if (!target) target = document.querySelector(attrs.slideToggle);
//                 if (!content) content = target.querySelector('.slideable_content');

//                 if(!attrs.expanded) {
//                     content.style.border = '1px solid rgba(0,0,0,0)';
//                     var y = content.clientHeight;
//                     content.style.border = 0;
//                     target.style.height = y + 'px';
//                 } else {
//                     target.style.height = '0px';
//                 }
//                 attrs.expanded = !attrs.expanded;
//             });
//         }
//     }
// });