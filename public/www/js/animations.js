window.Animations = {};

window.Animations.searchInit = function() {
  var morphSearch = document.getElementById('morphsearch'),
    input = morphSearch.querySelector('input.morphsearch-input'),
    ctrlClose = morphSearch.querySelector('span.morphsearch-close'),
    isOpen = isAnimating = false,
    // show/hide search area
    toggleSearch = function(evt) {
      // return if open and the input gets focused
      if (evt.type.toLowerCase() === 'focus' && isOpen) return false;

      var offsets = morphsearch.getBoundingClientRect();
      if (isOpen) {
        classie.remove(morphSearch, 'open');

        // trick to hide input text once the search overlay closes
        // todo: hardcoded times, should be done after transition ends
        if (input.value !== '') {
          setTimeout(function() {
            classie.add(morphSearch, 'hideInput');
            setTimeout(function() {
              classie.remove(morphSearch, 'hideInput');
              input.value = '';
            }, 300);
          }, 500);
        }

        input.blur();
      } else {
        classie.add(morphSearch, 'open');
      }
      isOpen = !isOpen;
    };

  // events
  input.addEventListener('focus', toggleSearch);
  ctrlClose.addEventListener('click', toggleSearch);
  // esc key closes search overlay
  // keyboard navigation events
  // document.addEventListener('keydown', function(ev) {
  //   var keyCode = ev.keyCode || ev.which;
  //   if (keyCode === 27 && isOpen) {
  //     toggleSearch(ev);
  //   }
  // });
};

window.Animations.inputsInit = function() {
  // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
  if (!String.prototype.trim) {
    (function() {
      // Make sure we trim BOM and NBSP
      var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
      String.prototype.trim = function() {
        return this.replace(rtrim, '');
      };
    })();
  }

  [].slice.call(document.querySelectorAll('input.input__field')).forEach(function(inputEl) {
    // in case the input is already filled..
    if (inputEl.value.trim() !== '') {
      classie.add(inputEl.parentNode, 'input--filled');
    }

    // events:
    inputEl.addEventListener('focus', onInputFocus);
    inputEl.addEventListener('blur', onInputBlur);
  });

  function onInputFocus(ev) {
    classie.add(ev.target.parentNode, 'input--filled');
  }

  function onInputBlur(ev) {
    if (ev.target.value.trim() === '') {
      classie.remove(ev.target.parentNode, 'input--filled');
    }
  }
};