Rails.application.routes.draw do

  devise_for :users
  namespace :api do
    resources :visits
    resources :visitors do
      collection do
        get 'validate_piv'
      end
    end
    resources :tenants do
      resources :organizers
      resources :guests
      resources :schedules
      resources :locations
    end
    resource :directory
    resource :auth do
      collection do
        get 'shortpath_user'
      end
    end
  end

end
